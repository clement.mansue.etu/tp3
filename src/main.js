import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

// console.log(document.querySelector('.logo img'));
// console.log(document.querySelector('.pizzaFormLink'));
// console.log(document.querySelector('h4'));

// console.log(document.querySelectorAll('.mainMenu a'));
// console.log(document.querySelectorAll('.pizzaThumbnail li'));

// const test = document.querySelectorAll('.pizzaThumbnail h4');
// console.log(test[1].innerHTML);

// const text = document.querySelector('.logo');
// console.log(text.innerHTML);
// text.innerHTML += `<small>les pizzas c'est la vie</small>`;

// const foot = document.querySelectorAll('footer div a');
// console.log(foot[1].getAttribute('href'));

// const carte = document.querySelector('.mainMenu li a');
// carte.setAttribute('class', `${carte.getAttribute('class')} active`);

const affichage = document.querySelector('.newsContainer');
affichage.setAttribute('style', `display:''`);

const button = document.querySelector('.closeButton');
button.addEventListener('click', event => {
	affichage.setAttribute('style', 'display:none');
});

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');
